> : move the tape forward the specified amount of cells
< : move the tape back the specified amount of cells (if <0, rollback to highest nonzero cell)
. : go to a specified cell
+ : add the specified amount to the current cell
- : subtract the specified amount to the current cell
V : set the current cell's value to the input (number or ASCII char code)
^ : output a character based off of using the current cell as an ASCII char code.
LOGIC : run logic gate operations on the previous two cells (odd/even as 0/1) overwriting the current cell with the output
, : skip to the corresponding ' in the program running the commands from that point
' : skip to the corresponding , in the program running the commands from that point, but only if a number is provided and it doesn't match the current cell

